PGDMP     .                    r           maiden    9.3.4    9.3.4 [   &           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            '           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            (           1262    16386    maiden    DATABASE     x   CREATE DATABASE maiden WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
    DROP DATABASE maiden;
             melissa    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            )           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    5            *           0    0    public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    5            �            3079    12670    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            +           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    238            �            1259    16414 
   auth_group    TABLE     ^   CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);
    DROP TABLE public.auth_group;
       public         melissa    false    5            �            1259    16412    auth_group_id_seq    SEQUENCE     s   CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.auth_group_id_seq;
       public       melissa    false    5    175            ,           0    0    auth_group_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;
            public       melissa    false    174            �            1259    16399    auth_group_permissions    TABLE     �   CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);
 *   DROP TABLE public.auth_group_permissions;
       public         melissa    false    5            �            1259    16397    auth_group_permissions_id_seq    SEQUENCE        CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.auth_group_permissions_id_seq;
       public       melissa    false    173    5            -           0    0    auth_group_permissions_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;
            public       melissa    false    172            �            1259    16389    auth_permission    TABLE     �   CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);
 #   DROP TABLE public.auth_permission;
       public         melissa    false    5            �            1259    16387    auth_permission_id_seq    SEQUENCE     x   CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.auth_permission_id_seq;
       public       melissa    false    171    5            .           0    0    auth_permission_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;
            public       melissa    false    170            �            1259    16459 	   auth_user    TABLE     �  CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);
    DROP TABLE public.auth_user;
       public         melissa    false    5            �            1259    16429    auth_user_groups    TABLE     x   CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);
 $   DROP TABLE public.auth_user_groups;
       public         melissa    false    5            �            1259    16427    auth_user_groups_id_seq    SEQUENCE     y   CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.auth_user_groups_id_seq;
       public       melissa    false    177    5            /           0    0    auth_user_groups_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;
            public       melissa    false    176            �            1259    16457    auth_user_id_seq    SEQUENCE     r   CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.auth_user_id_seq;
       public       melissa    false    181    5            0           0    0    auth_user_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;
            public       melissa    false    180            �            1259    16444    auth_user_user_permissions    TABLE     �   CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);
 .   DROP TABLE public.auth_user_user_permissions;
       public         melissa    false    5            �            1259    16442 !   auth_user_user_permissions_id_seq    SEQUENCE     �   CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.auth_user_user_permissions_id_seq;
       public       melissa    false    5    179            1           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;
            public       melissa    false    178            �            1259    16692    blog_blogcategory    TABLE     �   CREATE TABLE blog_blogcategory (
    id integer NOT NULL,
    site_id integer NOT NULL,
    title character varying(500) NOT NULL,
    slug character varying(2000)
);
 %   DROP TABLE public.blog_blogcategory;
       public         melissa    false    5            �            1259    16690    blog_blogcategory_id_seq    SEQUENCE     z   CREATE SEQUENCE blog_blogcategory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.blog_blogcategory_id_seq;
       public       melissa    false    209    5            2           0    0    blog_blogcategory_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE blog_blogcategory_id_seq OWNED BY blog_blogcategory.id;
            public       melissa    false    208            �            1259    16656    blog_blogpost    TABLE     j  CREATE TABLE blog_blogpost (
    id integer NOT NULL,
    comments_count integer NOT NULL,
    keywords_string character varying(500) NOT NULL,
    rating_count integer NOT NULL,
    rating_sum integer NOT NULL,
    rating_average double precision NOT NULL,
    site_id integer NOT NULL,
    title character varying(500) NOT NULL,
    slug character varying(2000),
    _meta_title character varying(500),
    description text NOT NULL,
    gen_description boolean NOT NULL,
    created timestamp with time zone,
    updated timestamp with time zone,
    status integer NOT NULL,
    publish_date timestamp with time zone,
    expiry_date timestamp with time zone,
    short_url character varying(200),
    in_sitemap boolean NOT NULL,
    content text NOT NULL,
    user_id integer NOT NULL,
    allow_comments boolean NOT NULL,
    featured_image character varying(255)
);
 !   DROP TABLE public.blog_blogpost;
       public         melissa    false    5            �            1259    16646    blog_blogpost_categories    TABLE     �   CREATE TABLE blog_blogpost_categories (
    id integer NOT NULL,
    blogpost_id integer NOT NULL,
    blogcategory_id integer NOT NULL
);
 ,   DROP TABLE public.blog_blogpost_categories;
       public         melissa    false    5            �            1259    16644    blog_blogpost_categories_id_seq    SEQUENCE     �   CREATE SEQUENCE blog_blogpost_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.blog_blogpost_categories_id_seq;
       public       melissa    false    5    205            3           0    0    blog_blogpost_categories_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE blog_blogpost_categories_id_seq OWNED BY blog_blogpost_categories.id;
            public       melissa    false    204            �            1259    16654    blog_blogpost_id_seq    SEQUENCE     v   CREATE SEQUENCE blog_blogpost_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.blog_blogpost_id_seq;
       public       melissa    false    5    207            4           0    0    blog_blogpost_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE blog_blogpost_id_seq OWNED BY blog_blogpost.id;
            public       melissa    false    206            �            1259    16636    blog_blogpost_related_posts    TABLE     �   CREATE TABLE blog_blogpost_related_posts (
    id integer NOT NULL,
    from_blogpost_id integer NOT NULL,
    to_blogpost_id integer NOT NULL
);
 /   DROP TABLE public.blog_blogpost_related_posts;
       public         melissa    false    5            �            1259    16634 "   blog_blogpost_related_posts_id_seq    SEQUENCE     �   CREATE SEQUENCE blog_blogpost_related_posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.blog_blogpost_related_posts_id_seq;
       public       melissa    false    203    5            5           0    0 "   blog_blogpost_related_posts_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE blog_blogpost_related_posts_id_seq OWNED BY blog_blogpost_related_posts.id;
            public       melissa    false    202            �            1259    16525    conf_setting    TABLE     �   CREATE TABLE conf_setting (
    id integer NOT NULL,
    site_id integer NOT NULL,
    name character varying(50) NOT NULL,
    value character varying(2000) NOT NULL
);
     DROP TABLE public.conf_setting;
       public         melissa    false    5            �            1259    16523    conf_setting_id_seq    SEQUENCE     u   CREATE SEQUENCE conf_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.conf_setting_id_seq;
       public       melissa    false    190    5            6           0    0    conf_setting_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE conf_setting_id_seq OWNED BY conf_setting.id;
            public       melissa    false    189            �            1259    16556    core_sitepermission    TABLE     \   CREATE TABLE core_sitepermission (
    id integer NOT NULL,
    user_id integer NOT NULL
);
 '   DROP TABLE public.core_sitepermission;
       public         melissa    false    5            �            1259    16554    core_sitepermission_id_seq    SEQUENCE     |   CREATE SEQUENCE core_sitepermission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.core_sitepermission_id_seq;
       public       melissa    false    194    5            7           0    0    core_sitepermission_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE core_sitepermission_id_seq OWNED BY core_sitepermission.id;
            public       melissa    false    193            �            1259    16541    core_sitepermission_sites    TABLE     �   CREATE TABLE core_sitepermission_sites (
    id integer NOT NULL,
    sitepermission_id integer NOT NULL,
    site_id integer NOT NULL
);
 -   DROP TABLE public.core_sitepermission_sites;
       public         melissa    false    5            �            1259    16539     core_sitepermission_sites_id_seq    SEQUENCE     �   CREATE SEQUENCE core_sitepermission_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.core_sitepermission_sites_id_seq;
       public       melissa    false    5    192            8           0    0     core_sitepermission_sites_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE core_sitepermission_sites_id_seq OWNED BY core_sitepermission_sites.id;
            public       melissa    false    191            �            1259    16879    django_admin_log    TABLE     �  CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);
 $   DROP TABLE public.django_admin_log;
       public         melissa    false    5            �            1259    16877    django_admin_log_id_seq    SEQUENCE     y   CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.django_admin_log_id_seq;
       public       melissa    false    5    231            9           0    0    django_admin_log_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;
            public       melissa    false    230            �            1259    16932    django_comment_flags    TABLE     �   CREATE TABLE django_comment_flags (
    id integer NOT NULL,
    user_id integer NOT NULL,
    comment_id integer NOT NULL,
    flag character varying(30) NOT NULL,
    flag_date timestamp with time zone NOT NULL
);
 (   DROP TABLE public.django_comment_flags;
       public         melissa    false    5            �            1259    16930    django_comment_flags_id_seq    SEQUENCE     }   CREATE SEQUENCE django_comment_flags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.django_comment_flags_id_seq;
       public       melissa    false    235    5            :           0    0    django_comment_flags_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE django_comment_flags_id_seq OWNED BY django_comment_flags.id;
            public       melissa    false    234            �            1259    16901    django_comments    TABLE     �  CREATE TABLE django_comments (
    id integer NOT NULL,
    content_type_id integer NOT NULL,
    object_pk text NOT NULL,
    site_id integer NOT NULL,
    user_id integer,
    user_name character varying(50) NOT NULL,
    user_email character varying(75) NOT NULL,
    user_url character varying(200) NOT NULL,
    comment text NOT NULL,
    submit_date timestamp with time zone NOT NULL,
    ip_address inet,
    is_public boolean NOT NULL,
    is_removed boolean NOT NULL
);
 #   DROP TABLE public.django_comments;
       public         melissa    false    5            �            1259    16899    django_comments_id_seq    SEQUENCE     x   CREATE SEQUENCE django_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.django_comments_id_seq;
       public       melissa    false    5    233            ;           0    0    django_comments_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE django_comments_id_seq OWNED BY django_comments.id;
            public       melissa    false    232            �            1259    16479    django_content_type    TABLE     �   CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);
 '   DROP TABLE public.django_content_type;
       public         melissa    false    5            �            1259    16477    django_content_type_id_seq    SEQUENCE     |   CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.django_content_type_id_seq;
       public       melissa    false    5    183            <           0    0    django_content_type_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;
            public       melissa    false    182            �            1259    16494    django_redirect    TABLE     �   CREATE TABLE django_redirect (
    id integer NOT NULL,
    site_id integer NOT NULL,
    old_path character varying(200) NOT NULL,
    new_path character varying(200) NOT NULL
);
 #   DROP TABLE public.django_redirect;
       public         melissa    false    5            �            1259    16492    django_redirect_id_seq    SEQUENCE     x   CREATE SEQUENCE django_redirect_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.django_redirect_id_seq;
       public       melissa    false    5    185            =           0    0    django_redirect_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE django_redirect_id_seq OWNED BY django_redirect.id;
            public       melissa    false    184            �            1259    16502    django_session    TABLE     �   CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);
 "   DROP TABLE public.django_session;
       public         melissa    false    5            �            1259    16512    django_site    TABLE     �   CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);
    DROP TABLE public.django_site;
       public         melissa    false    5            �            1259    16510    django_site_id_seq    SEQUENCE     t   CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.django_site_id_seq;
       public       melissa    false    5    188            >           0    0    django_site_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;
            public       melissa    false    187            �            1259    16721    forms_field    TABLE     �  CREATE TABLE forms_field (
    id integer NOT NULL,
    _order integer,
    form_id integer NOT NULL,
    label character varying(200) NOT NULL,
    field_type integer NOT NULL,
    required boolean NOT NULL,
    visible boolean NOT NULL,
    choices character varying(1000) NOT NULL,
    "default" character varying(2000) NOT NULL,
    placeholder_text character varying(100) NOT NULL,
    help_text character varying(100) NOT NULL
);
    DROP TABLE public.forms_field;
       public         melissa    false    5            �            1259    16719    forms_field_id_seq    SEQUENCE     t   CREATE SEQUENCE forms_field_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.forms_field_id_seq;
       public       melissa    false    5    212            ?           0    0    forms_field_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE forms_field_id_seq OWNED BY forms_field.id;
            public       melissa    false    211            �            1259    16750    forms_fieldentry    TABLE     �   CREATE TABLE forms_fieldentry (
    id integer NOT NULL,
    entry_id integer NOT NULL,
    field_id integer NOT NULL,
    value character varying(2000)
);
 $   DROP TABLE public.forms_fieldentry;
       public         melissa    false    5            �            1259    16748    forms_fieldentry_id_seq    SEQUENCE     y   CREATE SEQUENCE forms_fieldentry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.forms_fieldentry_id_seq;
       public       melissa    false    216    5            @           0    0    forms_fieldentry_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE forms_fieldentry_id_seq OWNED BY forms_fieldentry.id;
            public       melissa    false    215            �            1259    16711 
   forms_form    TABLE     {  CREATE TABLE forms_form (
    page_ptr_id integer NOT NULL,
    content text NOT NULL,
    button_text character varying(50) NOT NULL,
    response text NOT NULL,
    send_email boolean NOT NULL,
    email_from character varying(75) NOT NULL,
    email_copies character varying(200) NOT NULL,
    email_subject character varying(200) NOT NULL,
    email_message text NOT NULL
);
    DROP TABLE public.forms_form;
       public         melissa    false    5            �            1259    16737    forms_formentry    TABLE     �   CREATE TABLE forms_formentry (
    id integer NOT NULL,
    form_id integer NOT NULL,
    entry_time timestamp with time zone NOT NULL
);
 #   DROP TABLE public.forms_formentry;
       public         melissa    false    5            �            1259    16735    forms_formentry_id_seq    SEQUENCE     x   CREATE SEQUENCE forms_formentry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.forms_formentry_id_seq;
       public       melissa    false    5    214            A           0    0    forms_formentry_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE forms_formentry_id_seq OWNED BY forms_formentry.id;
            public       melissa    false    213            �            1259    16813    galleries_gallery    TABLE     �   CREATE TABLE galleries_gallery (
    page_ptr_id integer NOT NULL,
    content text NOT NULL,
    zip_import character varying(100) NOT NULL
);
 %   DROP TABLE public.galleries_gallery;
       public         melissa    false    5            �            1259    16828    galleries_galleryimage    TABLE     �   CREATE TABLE galleries_galleryimage (
    id integer NOT NULL,
    _order integer,
    gallery_id integer NOT NULL,
    file character varying(200) NOT NULL,
    description character varying(1000) NOT NULL
);
 *   DROP TABLE public.galleries_galleryimage;
       public         melissa    false    5            �            1259    16826    galleries_galleryimage_id_seq    SEQUENCE        CREATE SEQUENCE galleries_galleryimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.galleries_galleryimage_id_seq;
       public       melissa    false    5    223            B           0    0    galleries_galleryimage_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE galleries_galleryimage_id_seq OWNED BY galleries_galleryimage.id;
            public       melissa    false    222            �            1259    16600    generic_assignedkeyword    TABLE     �   CREATE TABLE generic_assignedkeyword (
    id integer NOT NULL,
    _order integer,
    keyword_id integer NOT NULL,
    content_type_id integer NOT NULL,
    object_pk integer NOT NULL
);
 +   DROP TABLE public.generic_assignedkeyword;
       public         melissa    false    5            �            1259    16598    generic_assignedkeyword_id_seq    SEQUENCE     �   CREATE SEQUENCE generic_assignedkeyword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public.generic_assignedkeyword_id_seq;
       public       melissa    false    199    5            C           0    0    generic_assignedkeyword_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE generic_assignedkeyword_id_seq OWNED BY generic_assignedkeyword.id;
            public       melissa    false    198            �            1259    16584    generic_keyword    TABLE     �   CREATE TABLE generic_keyword (
    id integer NOT NULL,
    site_id integer NOT NULL,
    title character varying(500) NOT NULL,
    slug character varying(2000)
);
 #   DROP TABLE public.generic_keyword;
       public         melissa    false    5            �            1259    16582    generic_keyword_id_seq    SEQUENCE     x   CREATE SEQUENCE generic_keyword_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.generic_keyword_id_seq;
       public       melissa    false    197    5            D           0    0    generic_keyword_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE generic_keyword_id_seq OWNED BY generic_keyword.id;
            public       melissa    false    196            �            1259    16618    generic_rating    TABLE     �   CREATE TABLE generic_rating (
    id integer NOT NULL,
    value integer NOT NULL,
    rating_date timestamp with time zone,
    content_type_id integer NOT NULL,
    object_pk integer NOT NULL,
    user_id integer
);
 "   DROP TABLE public.generic_rating;
       public         melissa    false    5            �            1259    16616    generic_rating_id_seq    SEQUENCE     w   CREATE SEQUENCE generic_rating_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.generic_rating_id_seq;
       public       melissa    false    5    201            E           0    0    generic_rating_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE generic_rating_id_seq OWNED BY generic_rating.id;
            public       melissa    false    200            �            1259    16572    generic_threadedcomment    TABLE     �   CREATE TABLE generic_threadedcomment (
    comment_ptr_id integer NOT NULL,
    rating_count integer NOT NULL,
    rating_sum integer NOT NULL,
    rating_average double precision NOT NULL,
    by_author boolean NOT NULL,
    replied_to_id integer
);
 +   DROP TABLE public.generic_threadedcomment;
       public         melissa    false    5            �            1259    17617    mezzanine_events_event    TABLE     �  CREATE TABLE mezzanine_events_event (
    page_ptr_id integer NOT NULL,
    content text NOT NULL,
    date date NOT NULL,
    start_time time without time zone NOT NULL,
    end_time time without time zone NOT NULL,
    speakers text NOT NULL,
    location text NOT NULL,
    mappable_location character varying(128) NOT NULL,
    lat numeric(10,7),
    lon numeric(10,7),
    rsvp text NOT NULL
);
 *   DROP TABLE public.mezzanine_events_event;
       public         melissa    false    5            �            1259    17625    mezzanine_events_eventcontainer    TABLE     w   CREATE TABLE mezzanine_events_eventcontainer (
    page_ptr_id integer NOT NULL,
    hide_children boolean NOT NULL
);
 3   DROP TABLE public.mezzanine_events_eventcontainer;
       public         melissa    false    5            �            1259    16803 
   pages_link    TABLE     >   CREATE TABLE pages_link (
    page_ptr_id integer NOT NULL
);
    DROP TABLE public.pages_link;
       public         melissa    false    5            �            1259    16766 
   pages_page    TABLE     	  CREATE TABLE pages_page (
    id integer NOT NULL,
    keywords_string character varying(500) NOT NULL,
    site_id integer NOT NULL,
    title character varying(500) NOT NULL,
    slug character varying(2000),
    _meta_title character varying(500),
    description text NOT NULL,
    gen_description boolean NOT NULL,
    created timestamp with time zone,
    updated timestamp with time zone,
    status integer NOT NULL,
    publish_date timestamp with time zone,
    expiry_date timestamp with time zone,
    short_url character varying(200),
    in_sitemap boolean NOT NULL,
    _order integer,
    parent_id integer,
    in_menus character varying(100),
    titles character varying(1000),
    content_model character varying(50),
    login_required boolean NOT NULL
);
    DROP TABLE public.pages_page;
       public         melissa    false    5            �            1259    16764    pages_page_id_seq    SEQUENCE     s   CREATE SEQUENCE pages_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.pages_page_id_seq;
       public       melissa    false    5    218            F           0    0    pages_page_id_seq    SEQUENCE OWNED BY     9   ALTER SEQUENCE pages_page_id_seq OWNED BY pages_page.id;
            public       melissa    false    217            �            1259    16790    pages_richtextpage    TABLE     a   CREATE TABLE pages_richtextpage (
    page_ptr_id integer NOT NULL,
    content text NOT NULL
);
 &   DROP TABLE public.pages_richtextpage;
       public         melissa    false    5            �            1259    16868    south_migrationhistory    TABLE     �   CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);
 *   DROP TABLE public.south_migrationhistory;
       public         melissa    false    5            �            1259    16866    south_migrationhistory_id_seq    SEQUENCE        CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public.south_migrationhistory_id_seq;
       public       melissa    false    5    229            G           0    0    south_migrationhistory_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;
            public       melissa    false    228            �            1259    16844    twitter_query    TABLE     �   CREATE TABLE twitter_query (
    id integer NOT NULL,
    type character varying(10) NOT NULL,
    value character varying(140) NOT NULL,
    interested boolean NOT NULL
);
 !   DROP TABLE public.twitter_query;
       public         melissa    false    5            �            1259    16842    twitter_query_id_seq    SEQUENCE     v   CREATE SEQUENCE twitter_query_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.twitter_query_id_seq;
       public       melissa    false    5    225            H           0    0    twitter_query_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE twitter_query_id_seq OWNED BY twitter_query.id;
            public       melissa    false    224            �            1259    16852    twitter_tweet    TABLE     �  CREATE TABLE twitter_tweet (
    id integer NOT NULL,
    remote_id character varying(50) NOT NULL,
    created_at timestamp with time zone,
    text text,
    profile_image_url character varying(200),
    user_name character varying(100),
    full_name character varying(100),
    retweeter_profile_image_url character varying(200),
    retweeter_user_name character varying(100),
    retweeter_full_name character varying(100),
    query_id integer NOT NULL
);
 !   DROP TABLE public.twitter_tweet;
       public         melissa    false    5            �            1259    16850    twitter_tweet_id_seq    SEQUENCE     v   CREATE SEQUENCE twitter_tweet_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.twitter_tweet_id_seq;
       public       melissa    false    5    227            I           0    0    twitter_tweet_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE twitter_tweet_id_seq OWNED BY twitter_tweet.id;
            public       melissa    false    226            �           2604    16417    id    DEFAULT     `   ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);
 <   ALTER TABLE public.auth_group ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    174    175    175            �           2604    16402    id    DEFAULT     x   ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);
 H   ALTER TABLE public.auth_group_permissions ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    172    173    173            �           2604    16392    id    DEFAULT     j   ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);
 A   ALTER TABLE public.auth_permission ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    171    170    171            �           2604    16462    id    DEFAULT     ^   ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);
 ;   ALTER TABLE public.auth_user ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    181    180    181            �           2604    16432    id    DEFAULT     l   ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);
 B   ALTER TABLE public.auth_user_groups ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    176    177    177            �           2604    16447    id    DEFAULT     �   ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);
 L   ALTER TABLE public.auth_user_user_permissions ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    179    178    179            �           2604    16695    id    DEFAULT     n   ALTER TABLE ONLY blog_blogcategory ALTER COLUMN id SET DEFAULT nextval('blog_blogcategory_id_seq'::regclass);
 C   ALTER TABLE public.blog_blogcategory ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    208    209    209            �           2604    16659    id    DEFAULT     f   ALTER TABLE ONLY blog_blogpost ALTER COLUMN id SET DEFAULT nextval('blog_blogpost_id_seq'::regclass);
 ?   ALTER TABLE public.blog_blogpost ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    206    207    207            �           2604    16649    id    DEFAULT     |   ALTER TABLE ONLY blog_blogpost_categories ALTER COLUMN id SET DEFAULT nextval('blog_blogpost_categories_id_seq'::regclass);
 J   ALTER TABLE public.blog_blogpost_categories ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    204    205    205            �           2604    16639    id    DEFAULT     �   ALTER TABLE ONLY blog_blogpost_related_posts ALTER COLUMN id SET DEFAULT nextval('blog_blogpost_related_posts_id_seq'::regclass);
 M   ALTER TABLE public.blog_blogpost_related_posts ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    202    203    203            �           2604    16528    id    DEFAULT     d   ALTER TABLE ONLY conf_setting ALTER COLUMN id SET DEFAULT nextval('conf_setting_id_seq'::regclass);
 >   ALTER TABLE public.conf_setting ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    190    189    190            �           2604    16559    id    DEFAULT     r   ALTER TABLE ONLY core_sitepermission ALTER COLUMN id SET DEFAULT nextval('core_sitepermission_id_seq'::regclass);
 E   ALTER TABLE public.core_sitepermission ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    194    193    194            �           2604    16544    id    DEFAULT     ~   ALTER TABLE ONLY core_sitepermission_sites ALTER COLUMN id SET DEFAULT nextval('core_sitepermission_sites_id_seq'::regclass);
 K   ALTER TABLE public.core_sitepermission_sites ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    192    191    192            �           2604    16882    id    DEFAULT     l   ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);
 B   ALTER TABLE public.django_admin_log ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    231    230    231            �           2604    16935    id    DEFAULT     t   ALTER TABLE ONLY django_comment_flags ALTER COLUMN id SET DEFAULT nextval('django_comment_flags_id_seq'::regclass);
 F   ALTER TABLE public.django_comment_flags ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    235    234    235            �           2604    16904    id    DEFAULT     j   ALTER TABLE ONLY django_comments ALTER COLUMN id SET DEFAULT nextval('django_comments_id_seq'::regclass);
 A   ALTER TABLE public.django_comments ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    233    232    233            �           2604    16482    id    DEFAULT     r   ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);
 E   ALTER TABLE public.django_content_type ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    183    182    183            �           2604    16497    id    DEFAULT     j   ALTER TABLE ONLY django_redirect ALTER COLUMN id SET DEFAULT nextval('django_redirect_id_seq'::regclass);
 A   ALTER TABLE public.django_redirect ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    184    185    185            �           2604    16515    id    DEFAULT     b   ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);
 =   ALTER TABLE public.django_site ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    187    188    188            �           2604    16724    id    DEFAULT     b   ALTER TABLE ONLY forms_field ALTER COLUMN id SET DEFAULT nextval('forms_field_id_seq'::regclass);
 =   ALTER TABLE public.forms_field ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    211    212    212            �           2604    16753    id    DEFAULT     l   ALTER TABLE ONLY forms_fieldentry ALTER COLUMN id SET DEFAULT nextval('forms_fieldentry_id_seq'::regclass);
 B   ALTER TABLE public.forms_fieldentry ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    215    216    216            �           2604    16740    id    DEFAULT     j   ALTER TABLE ONLY forms_formentry ALTER COLUMN id SET DEFAULT nextval('forms_formentry_id_seq'::regclass);
 A   ALTER TABLE public.forms_formentry ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    214    213    214            �           2604    16831    id    DEFAULT     x   ALTER TABLE ONLY galleries_galleryimage ALTER COLUMN id SET DEFAULT nextval('galleries_galleryimage_id_seq'::regclass);
 H   ALTER TABLE public.galleries_galleryimage ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    223    222    223            �           2604    16603    id    DEFAULT     z   ALTER TABLE ONLY generic_assignedkeyword ALTER COLUMN id SET DEFAULT nextval('generic_assignedkeyword_id_seq'::regclass);
 I   ALTER TABLE public.generic_assignedkeyword ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    199    198    199            �           2604    16587    id    DEFAULT     j   ALTER TABLE ONLY generic_keyword ALTER COLUMN id SET DEFAULT nextval('generic_keyword_id_seq'::regclass);
 A   ALTER TABLE public.generic_keyword ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    197    196    197            �           2604    16621    id    DEFAULT     h   ALTER TABLE ONLY generic_rating ALTER COLUMN id SET DEFAULT nextval('generic_rating_id_seq'::regclass);
 @   ALTER TABLE public.generic_rating ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    200    201    201            �           2604    16769    id    DEFAULT     `   ALTER TABLE ONLY pages_page ALTER COLUMN id SET DEFAULT nextval('pages_page_id_seq'::regclass);
 <   ALTER TABLE public.pages_page ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    217    218    218            �           2604    16871    id    DEFAULT     x   ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);
 H   ALTER TABLE public.south_migrationhistory ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    229    228    229            �           2604    16847    id    DEFAULT     f   ALTER TABLE ONLY twitter_query ALTER COLUMN id SET DEFAULT nextval('twitter_query_id_seq'::regclass);
 ?   ALTER TABLE public.twitter_query ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    225    224    225            �           2604    16855    id    DEFAULT     f   ALTER TABLE ONLY twitter_tweet ALTER COLUMN id SET DEFAULT nextval('twitter_tweet_id_seq'::regclass);
 ?   ALTER TABLE public.twitter_tweet ALTER COLUMN id DROP DEFAULT;
       public       melissa    false    226    227    227            �          0    16414 
   auth_group 
   TABLE DATA               '   COPY auth_group (id, name) FROM stdin;
    public       melissa    false    175   ��      J           0    0    auth_group_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('auth_group_id_seq', 1, false);
            public       melissa    false    174            �          0    16399    auth_group_permissions 
   TABLE DATA               F   COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
    public       melissa    false    173   ��      K           0    0    auth_group_permissions_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);
            public       melissa    false    172            �          0    16389    auth_permission 
   TABLE DATA               G   COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
    public       melissa    false    171   ʲ      L           0    0    auth_permission_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('auth_permission_id_seq', 97, true);
            public       melissa    false    170            �          0    16459 	   auth_user 
   TABLE DATA               �   COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
    public       melissa    false    181   Ҷ      �          0    16429    auth_user_groups 
   TABLE DATA               :   COPY auth_user_groups (id, user_id, group_id) FROM stdin;
    public       melissa    false    177   ��      M           0    0    auth_user_groups_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);
            public       melissa    false    176            N           0    0    auth_user_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('auth_user_id_seq', 1, true);
            public       melissa    false    180            �          0    16444    auth_user_user_permissions 
   TABLE DATA               I   COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
    public       melissa    false    179   ��      O           0    0 !   auth_user_user_permissions_id_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);
            public       melissa    false    178                      0    16692    blog_blogcategory 
   TABLE DATA               >   COPY blog_blogcategory (id, site_id, title, slug) FROM stdin;
    public       melissa    false    209   ��      P           0    0    blog_blogcategory_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('blog_blogcategory_id_seq', 1, false);
            public       melissa    false    208                      0    16656    blog_blogpost 
   TABLE DATA               .  COPY blog_blogpost (id, comments_count, keywords_string, rating_count, rating_sum, rating_average, site_id, title, slug, _meta_title, description, gen_description, created, updated, status, publish_date, expiry_date, short_url, in_sitemap, content, user_id, allow_comments, featured_image) FROM stdin;
    public       melissa    false    207   ޷                0    16646    blog_blogpost_categories 
   TABLE DATA               M   COPY blog_blogpost_categories (id, blogpost_id, blogcategory_id) FROM stdin;
    public       melissa    false    205   X�      Q           0    0    blog_blogpost_categories_id_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('blog_blogpost_categories_id_seq', 1, false);
            public       melissa    false    204            R           0    0    blog_blogpost_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('blog_blogpost_id_seq', 1, true);
            public       melissa    false    206                      0    16636    blog_blogpost_related_posts 
   TABLE DATA               T   COPY blog_blogpost_related_posts (id, from_blogpost_id, to_blogpost_id) FROM stdin;
    public       melissa    false    203   u�      S           0    0 "   blog_blogpost_related_posts_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('blog_blogpost_related_posts_id_seq', 1, false);
            public       melissa    false    202            �          0    16525    conf_setting 
   TABLE DATA               9   COPY conf_setting (id, site_id, name, value) FROM stdin;
    public       melissa    false    190   ��      T           0    0    conf_setting_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('conf_setting_id_seq', 33, true);
            public       melissa    false    189            �          0    16556    core_sitepermission 
   TABLE DATA               3   COPY core_sitepermission (id, user_id) FROM stdin;
    public       melissa    false    194   S�      U           0    0    core_sitepermission_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('core_sitepermission_id_seq', 1, false);
            public       melissa    false    193            �          0    16541    core_sitepermission_sites 
   TABLE DATA               L   COPY core_sitepermission_sites (id, sitepermission_id, site_id) FROM stdin;
    public       melissa    false    192   p�      V           0    0     core_sitepermission_sites_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('core_sitepermission_sites_id_seq', 1, false);
            public       melissa    false    191                      0    16879    django_admin_log 
   TABLE DATA               �   COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
    public       melissa    false    231   ��      W           0    0    django_admin_log_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('django_admin_log_id_seq', 36, true);
            public       melissa    false    230            !          0    16932    django_comment_flags 
   TABLE DATA               Q   COPY django_comment_flags (id, user_id, comment_id, flag, flag_date) FROM stdin;
    public       melissa    false    235   ��      X           0    0    django_comment_flags_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('django_comment_flags_id_seq', 1, false);
            public       melissa    false    234                      0    16901    django_comments 
   TABLE DATA               �   COPY django_comments (id, content_type_id, object_pk, site_id, user_id, user_name, user_email, user_url, comment, submit_date, ip_address, is_public, is_removed) FROM stdin;
    public       melissa    false    233   پ      Y           0    0    django_comments_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('django_comments_id_seq', 1, false);
            public       melissa    false    232            �          0    16479    django_content_type 
   TABLE DATA               B   COPY django_content_type (id, name, app_label, model) FROM stdin;
    public       melissa    false    183   ��      Z           0    0    django_content_type_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('django_content_type_id_seq', 32, true);
            public       melissa    false    182            �          0    16494    django_redirect 
   TABLE DATA               C   COPY django_redirect (id, site_id, old_path, new_path) FROM stdin;
    public       melissa    false    185   ��      [           0    0    django_redirect_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('django_redirect_id_seq', 1, false);
            public       melissa    false    184            �          0    16502    django_session 
   TABLE DATA               I   COPY django_session (session_key, session_data, expire_date) FROM stdin;
    public       melissa    false    186   ��      �          0    16512    django_site 
   TABLE DATA               0   COPY django_site (id, domain, name) FROM stdin;
    public       melissa    false    188   ]�      \           0    0    django_site_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('django_site_id_seq', 1, true);
            public       melissa    false    187            
          0    16721    forms_field 
   TABLE DATA               �   COPY forms_field (id, _order, form_id, label, field_type, required, visible, choices, "default", placeholder_text, help_text) FROM stdin;
    public       melissa    false    212   ��      ]           0    0    forms_field_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('forms_field_id_seq', 1, false);
            public       melissa    false    211                      0    16750    forms_fieldentry 
   TABLE DATA               B   COPY forms_fieldentry (id, entry_id, field_id, value) FROM stdin;
    public       melissa    false    216   ��      ^           0    0    forms_fieldentry_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('forms_fieldentry_id_seq', 1, false);
            public       melissa    false    215                      0    16711 
   forms_form 
   TABLE DATA               �   COPY forms_form (page_ptr_id, content, button_text, response, send_email, email_from, email_copies, email_subject, email_message) FROM stdin;
    public       melissa    false    210   ��                0    16737    forms_formentry 
   TABLE DATA               ;   COPY forms_formentry (id, form_id, entry_time) FROM stdin;
    public       melissa    false    214   ��      _           0    0    forms_formentry_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('forms_formentry_id_seq', 1, false);
            public       melissa    false    213                      0    16813    galleries_gallery 
   TABLE DATA               F   COPY galleries_gallery (page_ptr_id, content, zip_import) FROM stdin;
    public       melissa    false    221   �                0    16828    galleries_galleryimage 
   TABLE DATA               T   COPY galleries_galleryimage (id, _order, gallery_id, file, description) FROM stdin;
    public       melissa    false    223   "�      `           0    0    galleries_galleryimage_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('galleries_galleryimage_id_seq', 1, false);
            public       melissa    false    222            �          0    16600    generic_assignedkeyword 
   TABLE DATA               ^   COPY generic_assignedkeyword (id, _order, keyword_id, content_type_id, object_pk) FROM stdin;
    public       melissa    false    199   ?�      a           0    0    generic_assignedkeyword_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('generic_assignedkeyword_id_seq', 1, false);
            public       melissa    false    198            �          0    16584    generic_keyword 
   TABLE DATA               <   COPY generic_keyword (id, site_id, title, slug) FROM stdin;
    public       melissa    false    197   \�      b           0    0    generic_keyword_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('generic_keyword_id_seq', 1, false);
            public       melissa    false    196            �          0    16618    generic_rating 
   TABLE DATA               ^   COPY generic_rating (id, value, rating_date, content_type_id, object_pk, user_id) FROM stdin;
    public       melissa    false    201   y�      c           0    0    generic_rating_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('generic_rating_id_seq', 1, false);
            public       melissa    false    200            �          0    16572    generic_threadedcomment 
   TABLE DATA               ~   COPY generic_threadedcomment (comment_ptr_id, rating_count, rating_sum, rating_average, by_author, replied_to_id) FROM stdin;
    public       melissa    false    195   ��      "          0    17617    mezzanine_events_event 
   TABLE DATA               �   COPY mezzanine_events_event (page_ptr_id, content, date, start_time, end_time, speakers, location, mappable_location, lat, lon, rsvp) FROM stdin;
    public       melissa    false    236   ��      #          0    17625    mezzanine_events_eventcontainer 
   TABLE DATA               N   COPY mezzanine_events_eventcontainer (page_ptr_id, hide_children) FROM stdin;
    public       melissa    false    237   ��                0    16803 
   pages_link 
   TABLE DATA               *   COPY pages_link (page_ptr_id) FROM stdin;
    public       melissa    false    220   $�                0    16766 
   pages_page 
   TABLE DATA                 COPY pages_page (id, keywords_string, site_id, title, slug, _meta_title, description, gen_description, created, updated, status, publish_date, expiry_date, short_url, in_sitemap, _order, parent_id, in_menus, titles, content_model, login_required) FROM stdin;
    public       melissa    false    218   A�      d           0    0    pages_page_id_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('pages_page_id_seq', 9, true);
            public       melissa    false    217                      0    16790    pages_richtextpage 
   TABLE DATA               ;   COPY pages_richtextpage (page_ptr_id, content) FROM stdin;
    public       melissa    false    219   d�                0    16868    south_migrationhistory 
   TABLE DATA               K   COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
    public       melissa    false    229   ��      e           0    0    south_migrationhistory_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('south_migrationhistory_id_seq', 67, true);
            public       melissa    false    228                      0    16844    twitter_query 
   TABLE DATA               =   COPY twitter_query (id, type, value, interested) FROM stdin;
    public       melissa    false    225   ��      f           0    0    twitter_query_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('twitter_query_id_seq', 2, true);
            public       melissa    false    224                      0    16852    twitter_tweet 
   TABLE DATA               �   COPY twitter_tweet (id, remote_id, created_at, text, profile_image_url, user_name, full_name, retweeter_profile_image_url, retweeter_user_name, retweeter_full_name, query_id) FROM stdin;
    public       melissa    false    227   %�      g           0    0    twitter_tweet_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('twitter_tweet_id_seq', 1, true);
            public       melissa    false    226            �           2606    16421    auth_group_name_key 
   CONSTRAINT     R   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);
 H   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_name_key;
       public         melissa    false    175    175            �           2606    16406 1   auth_group_permissions_group_id_permission_id_key 
   CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);
 r   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_group_id_permission_id_key;
       public         melissa    false    173    173    173            �           2606    16404    auth_group_permissions_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_pkey;
       public         melissa    false    173    173            �           2606    16419    auth_group_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.auth_group DROP CONSTRAINT auth_group_pkey;
       public         melissa    false    175    175            �           2606    16396 ,   auth_permission_content_type_id_codename_key 
   CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);
 f   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_content_type_id_codename_key;
       public         melissa    false    171    171    171            �           2606    16394    auth_permission_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT auth_permission_pkey;
       public         melissa    false    171    171            �           2606    16434    auth_user_groups_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_pkey;
       public         melissa    false    177    177            �           2606    16436 %   auth_user_groups_user_id_group_id_key 
   CONSTRAINT     w   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);
 `   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_user_id_group_id_key;
       public         melissa    false    177    177    177            �           2606    16464    auth_user_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_pkey;
       public         melissa    false    181    181            �           2606    16449    auth_user_user_permissions_pkey 
   CONSTRAINT     q   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);
 d   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_pkey;
       public         melissa    false    179    179            �           2606    16451 4   auth_user_user_permissions_user_id_permission_id_key 
   CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);
 y   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_user_id_permission_id_key;
       public         melissa    false    179    179    179            �           2606    16466    auth_user_username_key 
   CONSTRAINT     X   ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);
 J   ALTER TABLE ONLY public.auth_user DROP CONSTRAINT auth_user_username_key;
       public         melissa    false    181    181                       2606    16700    blog_blogcategory_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY blog_blogcategory
    ADD CONSTRAINT blog_blogcategory_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.blog_blogcategory DROP CONSTRAINT blog_blogcategory_pkey;
       public         melissa    false    209    209            	           2606    16653 8   blog_blogpost_categories_blogpost_id_blogcategory_id_key 
   CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_categories
    ADD CONSTRAINT blog_blogpost_categories_blogpost_id_blogcategory_id_key UNIQUE (blogpost_id, blogcategory_id);
 {   ALTER TABLE ONLY public.blog_blogpost_categories DROP CONSTRAINT blog_blogpost_categories_blogpost_id_blogcategory_id_key;
       public         melissa    false    205    205    205                       2606    16651    blog_blogpost_categories_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY blog_blogpost_categories
    ADD CONSTRAINT blog_blogpost_categories_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.blog_blogpost_categories DROP CONSTRAINT blog_blogpost_categories_pkey;
       public         melissa    false    205    205                       2606    16664    blog_blogpost_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY blog_blogpost
    ADD CONSTRAINT blog_blogpost_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.blog_blogpost DROP CONSTRAINT blog_blogpost_pkey;
       public         melissa    false    207    207                       2606    16643 ?   blog_blogpost_related_posts_from_blogpost_id_to_blogpost_id_key 
   CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_related_posts
    ADD CONSTRAINT blog_blogpost_related_posts_from_blogpost_id_to_blogpost_id_key UNIQUE (from_blogpost_id, to_blogpost_id);
 �   ALTER TABLE ONLY public.blog_blogpost_related_posts DROP CONSTRAINT blog_blogpost_related_posts_from_blogpost_id_to_blogpost_id_key;
       public         melissa    false    203    203    203                       2606    16641     blog_blogpost_related_posts_pkey 
   CONSTRAINT     s   ALTER TABLE ONLY blog_blogpost_related_posts
    ADD CONSTRAINT blog_blogpost_related_posts_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.blog_blogpost_related_posts DROP CONSTRAINT blog_blogpost_related_posts_pkey;
       public         melissa    false    203    203            �           2606    16533    conf_setting_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY conf_setting
    ADD CONSTRAINT conf_setting_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.conf_setting DROP CONSTRAINT conf_setting_pkey;
       public         melissa    false    190    190            �           2606    16561    core_sitepermission_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY core_sitepermission
    ADD CONSTRAINT core_sitepermission_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.core_sitepermission DROP CONSTRAINT core_sitepermission_pkey;
       public         melissa    false    194    194            �           2606    16546    core_sitepermission_sites_pkey 
   CONSTRAINT     o   ALTER TABLE ONLY core_sitepermission_sites
    ADD CONSTRAINT core_sitepermission_sites_pkey PRIMARY KEY (id);
 b   ALTER TABLE ONLY public.core_sitepermission_sites DROP CONSTRAINT core_sitepermission_sites_pkey;
       public         melissa    false    192    192            �           2606    16548 7   core_sitepermission_sites_sitepermission_id_site_id_key 
   CONSTRAINT     �   ALTER TABLE ONLY core_sitepermission_sites
    ADD CONSTRAINT core_sitepermission_sites_sitepermission_id_site_id_key UNIQUE (sitepermission_id, site_id);
 {   ALTER TABLE ONLY public.core_sitepermission_sites DROP CONSTRAINT core_sitepermission_sites_sitepermission_id_site_id_key;
       public         melissa    false    192    192    192            4           2606    16888    django_admin_log_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_pkey;
       public         melissa    false    231    231            ?           2606    16937    django_comment_flags_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.django_comment_flags DROP CONSTRAINT django_comment_flags_pkey;
       public         melissa    false    235    235            B           2606    16939 0   django_comment_flags_user_id_comment_id_flag_key 
   CONSTRAINT     �   ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_user_id_comment_id_flag_key UNIQUE (user_id, comment_id, flag);
 o   ALTER TABLE ONLY public.django_comment_flags DROP CONSTRAINT django_comment_flags_user_id_comment_id_flag_key;
       public         melissa    false    235    235    235    235            8           2606    16909    django_comments_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.django_comments DROP CONSTRAINT django_comments_pkey;
       public         melissa    false    233    233            �           2606    16486 '   django_content_type_app_label_model_key 
   CONSTRAINT     {   ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);
 e   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_app_label_model_key;
       public         melissa    false    183    183    183            �           2606    16484    django_content_type_pkey 
   CONSTRAINT     c   ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.django_content_type DROP CONSTRAINT django_content_type_pkey;
       public         melissa    false    183    183            �           2606    16499    django_redirect_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY django_redirect
    ADD CONSTRAINT django_redirect_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.django_redirect DROP CONSTRAINT django_redirect_pkey;
       public         melissa    false    185    185            �           2606    16501 $   django_redirect_site_id_old_path_key 
   CONSTRAINT     u   ALTER TABLE ONLY django_redirect
    ADD CONSTRAINT django_redirect_site_id_old_path_key UNIQUE (site_id, old_path);
 ^   ALTER TABLE ONLY public.django_redirect DROP CONSTRAINT django_redirect_site_id_old_path_key;
       public         melissa    false    185    185    185            �           2606    16509    django_session_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);
 L   ALTER TABLE ONLY public.django_session DROP CONSTRAINT django_session_pkey;
       public         melissa    false    186    186            �           2606    16517    django_site_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.django_site DROP CONSTRAINT django_site_pkey;
       public         melissa    false    188    188                       2606    16729    forms_field_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY forms_field
    ADD CONSTRAINT forms_field_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.forms_field DROP CONSTRAINT forms_field_pkey;
       public         melissa    false    212    212                       2606    16758    forms_fieldentry_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY forms_fieldentry
    ADD CONSTRAINT forms_fieldentry_pkey PRIMARY KEY (id);
 P   ALTER TABLE ONLY public.forms_fieldentry DROP CONSTRAINT forms_fieldentry_pkey;
       public         melissa    false    216    216                       2606    16718    forms_form_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY forms_form
    ADD CONSTRAINT forms_form_pkey PRIMARY KEY (page_ptr_id);
 D   ALTER TABLE ONLY public.forms_form DROP CONSTRAINT forms_form_pkey;
       public         melissa    false    210    210                       2606    16742    forms_formentry_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY forms_formentry
    ADD CONSTRAINT forms_formentry_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.forms_formentry DROP CONSTRAINT forms_formentry_pkey;
       public         melissa    false    214    214            '           2606    16820    galleries_gallery_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY galleries_gallery
    ADD CONSTRAINT galleries_gallery_pkey PRIMARY KEY (page_ptr_id);
 R   ALTER TABLE ONLY public.galleries_gallery DROP CONSTRAINT galleries_gallery_pkey;
       public         melissa    false    221    221            *           2606    16836    galleries_galleryimage_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY galleries_galleryimage
    ADD CONSTRAINT galleries_galleryimage_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.galleries_galleryimage DROP CONSTRAINT galleries_galleryimage_pkey;
       public         melissa    false    223    223            �           2606    16605    generic_assignedkeyword_pkey 
   CONSTRAINT     k   ALTER TABLE ONLY generic_assignedkeyword
    ADD CONSTRAINT generic_assignedkeyword_pkey PRIMARY KEY (id);
 ^   ALTER TABLE ONLY public.generic_assignedkeyword DROP CONSTRAINT generic_assignedkeyword_pkey;
       public         melissa    false    199    199            �           2606    16592    generic_keyword_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY generic_keyword
    ADD CONSTRAINT generic_keyword_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.generic_keyword DROP CONSTRAINT generic_keyword_pkey;
       public         melissa    false    197    197            �           2606    16623    generic_rating_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY generic_rating
    ADD CONSTRAINT generic_rating_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.generic_rating DROP CONSTRAINT generic_rating_pkey;
       public         melissa    false    201    201            �           2606    16576    generic_threadedcomment_pkey 
   CONSTRAINT     w   ALTER TABLE ONLY generic_threadedcomment
    ADD CONSTRAINT generic_threadedcomment_pkey PRIMARY KEY (comment_ptr_id);
 ^   ALTER TABLE ONLY public.generic_threadedcomment DROP CONSTRAINT generic_threadedcomment_pkey;
       public         melissa    false    195    195            D           2606    17624    mezzanine_events_event_pkey 
   CONSTRAINT     r   ALTER TABLE ONLY mezzanine_events_event
    ADD CONSTRAINT mezzanine_events_event_pkey PRIMARY KEY (page_ptr_id);
 \   ALTER TABLE ONLY public.mezzanine_events_event DROP CONSTRAINT mezzanine_events_event_pkey;
       public         melissa    false    236    236            F           2606    17629 $   mezzanine_events_eventcontainer_pkey 
   CONSTRAINT     �   ALTER TABLE ONLY mezzanine_events_eventcontainer
    ADD CONSTRAINT mezzanine_events_eventcontainer_pkey PRIMARY KEY (page_ptr_id);
 n   ALTER TABLE ONLY public.mezzanine_events_eventcontainer DROP CONSTRAINT mezzanine_events_eventcontainer_pkey;
       public         melissa    false    237    237            %           2606    16807    pages_link_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY pages_link
    ADD CONSTRAINT pages_link_pkey PRIMARY KEY (page_ptr_id);
 D   ALTER TABLE ONLY public.pages_link DROP CONSTRAINT pages_link_pkey;
       public         melissa    false    220    220                        2606    16774    pages_page_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.pages_page DROP CONSTRAINT pages_page_pkey;
       public         melissa    false    218    218            #           2606    16797    pages_richtextpage_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY pages_richtextpage
    ADD CONSTRAINT pages_richtextpage_pkey PRIMARY KEY (page_ptr_id);
 T   ALTER TABLE ONLY public.pages_richtextpage DROP CONSTRAINT pages_richtextpage_pkey;
       public         melissa    false    219    219            1           2606    16876    south_migrationhistory_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);
 \   ALTER TABLE ONLY public.south_migrationhistory DROP CONSTRAINT south_migrationhistory_pkey;
       public         melissa    false    229    229            ,           2606    16849    twitter_query_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY twitter_query
    ADD CONSTRAINT twitter_query_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.twitter_query DROP CONSTRAINT twitter_query_pkey;
       public         melissa    false    225    225            .           2606    16860    twitter_tweet_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY twitter_tweet
    ADD CONSTRAINT twitter_tweet_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.twitter_tweet DROP CONSTRAINT twitter_tweet_pkey;
       public         melissa    false    227    227            �           1259    16953    auth_group_name_like    INDEX     X   CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);
 (   DROP INDEX public.auth_group_name_like;
       public         melissa    false    175            �           1259    16951    auth_group_permissions_group_id    INDEX     _   CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);
 3   DROP INDEX public.auth_group_permissions_group_id;
       public         melissa    false    173            �           1259    16952 $   auth_group_permissions_permission_id    INDEX     i   CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);
 8   DROP INDEX public.auth_group_permissions_permission_id;
       public         melissa    false    173            �           1259    16950    auth_permission_content_type_id    INDEX     _   CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);
 3   DROP INDEX public.auth_permission_content_type_id;
       public         melissa    false    171            �           1259    16955    auth_user_groups_group_id    INDEX     S   CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);
 -   DROP INDEX public.auth_user_groups_group_id;
       public         melissa    false    177            �           1259    16954    auth_user_groups_user_id    INDEX     Q   CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);
 ,   DROP INDEX public.auth_user_groups_user_id;
       public         melissa    false    177            �           1259    16957 (   auth_user_user_permissions_permission_id    INDEX     q   CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);
 <   DROP INDEX public.auth_user_user_permissions_permission_id;
       public         melissa    false    179            �           1259    16956 "   auth_user_user_permissions_user_id    INDEX     e   CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);
 6   DROP INDEX public.auth_user_user_permissions_user_id;
       public         melissa    false    179            �           1259    16958    auth_user_username_like    INDEX     ^   CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);
 +   DROP INDEX public.auth_user_username_like;
       public         melissa    false    181                       1259    16980    blog_blogcategory_site_id    INDEX     S   CREATE INDEX blog_blogcategory_site_id ON blog_blogcategory USING btree (site_id);
 -   DROP INDEX public.blog_blogcategory_site_id;
       public         melissa    false    209                       1259    16977 (   blog_blogpost_categories_blogcategory_id    INDEX     q   CREATE INDEX blog_blogpost_categories_blogcategory_id ON blog_blogpost_categories USING btree (blogcategory_id);
 <   DROP INDEX public.blog_blogpost_categories_blogcategory_id;
       public         melissa    false    205                       1259    16976 $   blog_blogpost_categories_blogpost_id    INDEX     i   CREATE INDEX blog_blogpost_categories_blogpost_id ON blog_blogpost_categories USING btree (blogpost_id);
 8   DROP INDEX public.blog_blogpost_categories_blogpost_id;
       public         melissa    false    205                        1259    16974 ,   blog_blogpost_related_posts_from_blogpost_id    INDEX     y   CREATE INDEX blog_blogpost_related_posts_from_blogpost_id ON blog_blogpost_related_posts USING btree (from_blogpost_id);
 @   DROP INDEX public.blog_blogpost_related_posts_from_blogpost_id;
       public         melissa    false    203                       1259    16975 *   blog_blogpost_related_posts_to_blogpost_id    INDEX     u   CREATE INDEX blog_blogpost_related_posts_to_blogpost_id ON blog_blogpost_related_posts USING btree (to_blogpost_id);
 >   DROP INDEX public.blog_blogpost_related_posts_to_blogpost_id;
       public         melissa    false    203                       1259    16978    blog_blogpost_site_id    INDEX     K   CREATE INDEX blog_blogpost_site_id ON blog_blogpost USING btree (site_id);
 )   DROP INDEX public.blog_blogpost_site_id;
       public         melissa    false    207                       1259    16979    blog_blogpost_user_id    INDEX     K   CREATE INDEX blog_blogpost_user_id ON blog_blogpost USING btree (user_id);
 )   DROP INDEX public.blog_blogpost_user_id;
       public         melissa    false    207            �           1259    16964    conf_setting_site_id    INDEX     I   CREATE INDEX conf_setting_site_id ON conf_setting USING btree (site_id);
 (   DROP INDEX public.conf_setting_site_id;
       public         melissa    false    190            �           1259    16966 !   core_sitepermission_sites_site_id    INDEX     c   CREATE INDEX core_sitepermission_sites_site_id ON core_sitepermission_sites USING btree (site_id);
 5   DROP INDEX public.core_sitepermission_sites_site_id;
       public         melissa    false    192            �           1259    16965 +   core_sitepermission_sites_sitepermission_id    INDEX     w   CREATE INDEX core_sitepermission_sites_sitepermission_id ON core_sitepermission_sites USING btree (sitepermission_id);
 ?   DROP INDEX public.core_sitepermission_sites_sitepermission_id;
       public         melissa    false    192            �           1259    16967    core_sitepermission_user_id    INDEX     W   CREATE INDEX core_sitepermission_user_id ON core_sitepermission USING btree (user_id);
 /   DROP INDEX public.core_sitepermission_user_id;
       public         melissa    false    194            2           1259    16989     django_admin_log_content_type_id    INDEX     a   CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);
 4   DROP INDEX public.django_admin_log_content_type_id;
       public         melissa    false    231            5           1259    16988    django_admin_log_user_id    INDEX     Q   CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);
 ,   DROP INDEX public.django_admin_log_user_id;
       public         melissa    false    231            ;           1259    16994    django_comment_flags_comment_id    INDEX     _   CREATE INDEX django_comment_flags_comment_id ON django_comment_flags USING btree (comment_id);
 3   DROP INDEX public.django_comment_flags_comment_id;
       public         melissa    false    235            <           1259    16995    django_comment_flags_flag    INDEX     S   CREATE INDEX django_comment_flags_flag ON django_comment_flags USING btree (flag);
 -   DROP INDEX public.django_comment_flags_flag;
       public         melissa    false    235            =           1259    16996    django_comment_flags_flag_like    INDEX     l   CREATE INDEX django_comment_flags_flag_like ON django_comment_flags USING btree (flag varchar_pattern_ops);
 2   DROP INDEX public.django_comment_flags_flag_like;
       public         melissa    false    235            @           1259    16993    django_comment_flags_user_id    INDEX     Y   CREATE INDEX django_comment_flags_user_id ON django_comment_flags USING btree (user_id);
 0   DROP INDEX public.django_comment_flags_user_id;
       public         melissa    false    235            6           1259    16990    django_comments_content_type_id    INDEX     _   CREATE INDEX django_comments_content_type_id ON django_comments USING btree (content_type_id);
 3   DROP INDEX public.django_comments_content_type_id;
       public         melissa    false    233            9           1259    16991    django_comments_site_id    INDEX     O   CREATE INDEX django_comments_site_id ON django_comments USING btree (site_id);
 +   DROP INDEX public.django_comments_site_id;
       public         melissa    false    233            :           1259    16992    django_comments_user_id    INDEX     O   CREATE INDEX django_comments_user_id ON django_comments USING btree (user_id);
 +   DROP INDEX public.django_comments_user_id;
       public         melissa    false    233            �           1259    16960    django_redirect_old_path    INDEX     Q   CREATE INDEX django_redirect_old_path ON django_redirect USING btree (old_path);
 ,   DROP INDEX public.django_redirect_old_path;
       public         melissa    false    185            �           1259    16961    django_redirect_old_path_like    INDEX     j   CREATE INDEX django_redirect_old_path_like ON django_redirect USING btree (old_path varchar_pattern_ops);
 1   DROP INDEX public.django_redirect_old_path_like;
       public         melissa    false    185            �           1259    16959    django_redirect_site_id    INDEX     O   CREATE INDEX django_redirect_site_id ON django_redirect USING btree (site_id);
 +   DROP INDEX public.django_redirect_site_id;
       public         melissa    false    185            �           1259    16963    django_session_expire_date    INDEX     U   CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);
 .   DROP INDEX public.django_session_expire_date;
       public         melissa    false    186            �           1259    16962    django_session_session_key_like    INDEX     n   CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);
 3   DROP INDEX public.django_session_session_key_like;
       public         melissa    false    186                       1259    16981    forms_field_form_id    INDEX     G   CREATE INDEX forms_field_form_id ON forms_field USING btree (form_id);
 '   DROP INDEX public.forms_field_form_id;
       public         melissa    false    212                       1259    16983    forms_fieldentry_entry_id    INDEX     S   CREATE INDEX forms_fieldentry_entry_id ON forms_fieldentry USING btree (entry_id);
 -   DROP INDEX public.forms_fieldentry_entry_id;
       public         melissa    false    216                       1259    16982    forms_formentry_form_id    INDEX     O   CREATE INDEX forms_formentry_form_id ON forms_formentry USING btree (form_id);
 +   DROP INDEX public.forms_formentry_form_id;
       public         melissa    false    214            (           1259    16986 !   galleries_galleryimage_gallery_id    INDEX     c   CREATE INDEX galleries_galleryimage_gallery_id ON galleries_galleryimage USING btree (gallery_id);
 5   DROP INDEX public.galleries_galleryimage_gallery_id;
       public         melissa    false    223            �           1259    16971 '   generic_assignedkeyword_content_type_id    INDEX     o   CREATE INDEX generic_assignedkeyword_content_type_id ON generic_assignedkeyword USING btree (content_type_id);
 ;   DROP INDEX public.generic_assignedkeyword_content_type_id;
       public         melissa    false    199            �           1259    16970 "   generic_assignedkeyword_keyword_id    INDEX     e   CREATE INDEX generic_assignedkeyword_keyword_id ON generic_assignedkeyword USING btree (keyword_id);
 6   DROP INDEX public.generic_assignedkeyword_keyword_id;
       public         melissa    false    199            �           1259    16969    generic_keyword_site_id    INDEX     O   CREATE INDEX generic_keyword_site_id ON generic_keyword USING btree (site_id);
 +   DROP INDEX public.generic_keyword_site_id;
       public         melissa    false    197            �           1259    16972    generic_rating_content_type_id    INDEX     ]   CREATE INDEX generic_rating_content_type_id ON generic_rating USING btree (content_type_id);
 2   DROP INDEX public.generic_rating_content_type_id;
       public         melissa    false    201            �           1259    16973    generic_rating_user_id    INDEX     M   CREATE INDEX generic_rating_user_id ON generic_rating USING btree (user_id);
 *   DROP INDEX public.generic_rating_user_id;
       public         melissa    false    201            �           1259    16968 %   generic_threadedcomment_replied_to_id    INDEX     k   CREATE INDEX generic_threadedcomment_replied_to_id ON generic_threadedcomment USING btree (replied_to_id);
 9   DROP INDEX public.generic_threadedcomment_replied_to_id;
       public         melissa    false    195                       1259    16985    pages_page_parent_id    INDEX     I   CREATE INDEX pages_page_parent_id ON pages_page USING btree (parent_id);
 (   DROP INDEX public.pages_page_parent_id;
       public         melissa    false    218            !           1259    16984    pages_page_site_id    INDEX     E   CREATE INDEX pages_page_site_id ON pages_page USING btree (site_id);
 &   DROP INDEX public.pages_page_site_id;
       public         melissa    false    218            /           1259    16987    twitter_tweet_query_id    INDEX     M   CREATE INDEX twitter_tweet_query_id ON twitter_tweet USING btree (query_id);
 *   DROP INDEX public.twitter_tweet_query_id;
       public         melissa    false    227            H           2606    16407 )   auth_group_permissions_permission_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT auth_group_permissions_permission_id_fkey;
       public       melissa    false    173    171    3000            J           2606    16437    auth_user_groups_group_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 Y   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT auth_user_groups_group_id_fkey;
       public       melissa    false    177    175    3011            L           2606    16452 -   auth_user_user_permissions_permission_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;
 r   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT auth_user_user_permissions_permission_id_fkey;
       public       melissa    false    3000    179    171            `           2606    16701    blog_blogcategory_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogcategory
    ADD CONSTRAINT blog_blogcategory_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 Z   ALTER TABLE ONLY public.blog_blogcategory DROP CONSTRAINT blog_blogcategory_site_id_fkey;
       public       melissa    false    209    188    3045            ^           2606    16665    blog_blogpost_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost
    ADD CONSTRAINT blog_blogpost_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 R   ALTER TABLE ONLY public.blog_blogpost DROP CONSTRAINT blog_blogpost_site_id_fkey;
       public       melissa    false    207    188    3045            _           2606    16670    blog_blogpost_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost
    ADD CONSTRAINT blog_blogpost_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 R   ALTER TABLE ONLY public.blog_blogpost DROP CONSTRAINT blog_blogpost_user_id_fkey;
       public       melissa    false    207    181    3025            ]           2606    16706     blogcategory_id_refs_id_91693b1c    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_categories
    ADD CONSTRAINT blogcategory_id_refs_id_91693b1c FOREIGN KEY (blogcategory_id) REFERENCES blog_blogcategory(id) DEFERRABLE INITIALLY DEFERRED;
 c   ALTER TABLE ONLY public.blog_blogpost_categories DROP CONSTRAINT blogcategory_id_refs_id_91693b1c;
       public       melissa    false    205    209    3089            \           2606    16685    blogpost_id_refs_id_6a2ad936    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_categories
    ADD CONSTRAINT blogpost_id_refs_id_6a2ad936 FOREIGN KEY (blogpost_id) REFERENCES blog_blogpost(id) DEFERRABLE INITIALLY DEFERRED;
 _   ALTER TABLE ONLY public.blog_blogpost_categories DROP CONSTRAINT blogpost_id_refs_id_6a2ad936;
       public       melissa    false    205    207    3085            T           2606    16925    comment_ptr_id_refs_id_d4c241e5    FK CONSTRAINT     �   ALTER TABLE ONLY generic_threadedcomment
    ADD CONSTRAINT comment_ptr_id_refs_id_d4c241e5 FOREIGN KEY (comment_ptr_id) REFERENCES django_comments(id) DEFERRABLE INITIALLY DEFERRED;
 a   ALTER TABLE ONLY public.generic_threadedcomment DROP CONSTRAINT comment_ptr_id_refs_id_d4c241e5;
       public       melissa    false    3128    233    195            O           2606    16534    conf_setting_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY conf_setting
    ADD CONSTRAINT conf_setting_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 P   ALTER TABLE ONLY public.conf_setting DROP CONSTRAINT conf_setting_site_id_fkey;
       public       melissa    false    3045    190    188            G           2606    16487     content_type_id_refs_id_d043b34a    FK CONSTRAINT     �   ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 Z   ALTER TABLE ONLY public.auth_permission DROP CONSTRAINT content_type_id_refs_id_d043b34a;
       public       melissa    false    3032    171    183            P           2606    16549 &   core_sitepermission_sites_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY core_sitepermission_sites
    ADD CONSTRAINT core_sitepermission_sites_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 j   ALTER TABLE ONLY public.core_sitepermission_sites DROP CONSTRAINT core_sitepermission_sites_site_id_fkey;
       public       melissa    false    192    3045    188            R           2606    16562     core_sitepermission_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY core_sitepermission
    ADD CONSTRAINT core_sitepermission_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 ^   ALTER TABLE ONLY public.core_sitepermission DROP CONSTRAINT core_sitepermission_user_id_fkey;
       public       melissa    false    181    194    3025            m           2606    16894 %   django_admin_log_content_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 `   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_content_type_id_fkey;
       public       melissa    false    183    3032    231            l           2606    16889    django_admin_log_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 X   ALTER TABLE ONLY public.django_admin_log DROP CONSTRAINT django_admin_log_user_id_fkey;
       public       melissa    false    181    231    3025            r           2606    16945 $   django_comment_flags_comment_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_comment_id_fkey FOREIGN KEY (comment_id) REFERENCES django_comments(id) DEFERRABLE INITIALLY DEFERRED;
 c   ALTER TABLE ONLY public.django_comment_flags DROP CONSTRAINT django_comment_flags_comment_id_fkey;
       public       melissa    false    235    3128    233            q           2606    16940 !   django_comment_flags_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_comment_flags
    ADD CONSTRAINT django_comment_flags_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 `   ALTER TABLE ONLY public.django_comment_flags DROP CONSTRAINT django_comment_flags_user_id_fkey;
       public       melissa    false    235    3025    181            n           2606    16910 $   django_comments_content_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 ^   ALTER TABLE ONLY public.django_comments DROP CONSTRAINT django_comments_content_type_id_fkey;
       public       melissa    false    3032    233    183            o           2606    16915    django_comments_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 V   ALTER TABLE ONLY public.django_comments DROP CONSTRAINT django_comments_site_id_fkey;
       public       melissa    false    188    3045    233            p           2606    16920    django_comments_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY django_comments
    ADD CONSTRAINT django_comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 V   ALTER TABLE ONLY public.django_comments DROP CONSTRAINT django_comments_user_id_fkey;
       public       melissa    false    181    233    3025            b           2606    16730    forms_field_form_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY forms_field
    ADD CONSTRAINT forms_field_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms_form(page_ptr_id) DEFERRABLE INITIALLY DEFERRED;
 N   ALTER TABLE ONLY public.forms_field DROP CONSTRAINT forms_field_form_id_fkey;
       public       melissa    false    212    210    3092            d           2606    16759    forms_fieldentry_entry_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY forms_fieldentry
    ADD CONSTRAINT forms_fieldentry_entry_id_fkey FOREIGN KEY (entry_id) REFERENCES forms_formentry(id) DEFERRABLE INITIALLY DEFERRED;
 Y   ALTER TABLE ONLY public.forms_fieldentry DROP CONSTRAINT forms_fieldentry_entry_id_fkey;
       public       melissa    false    216    214    3098            c           2606    16743    forms_formentry_form_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY forms_formentry
    ADD CONSTRAINT forms_formentry_form_id_fkey FOREIGN KEY (form_id) REFERENCES forms_form(page_ptr_id) DEFERRABLE INITIALLY DEFERRED;
 V   ALTER TABLE ONLY public.forms_formentry DROP CONSTRAINT forms_formentry_form_id_fkey;
       public       melissa    false    214    210    3092            Z           2606    16675 !   from_blogpost_id_refs_id_6404941b    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_related_posts
    ADD CONSTRAINT from_blogpost_id_refs_id_6404941b FOREIGN KEY (from_blogpost_id) REFERENCES blog_blogpost(id) DEFERRABLE INITIALLY DEFERRED;
 g   ALTER TABLE ONLY public.blog_blogpost_related_posts DROP CONSTRAINT from_blogpost_id_refs_id_6404941b;
       public       melissa    false    203    207    3085            i           2606    16821 "   galleries_gallery_page_ptr_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY galleries_gallery
    ADD CONSTRAINT galleries_gallery_page_ptr_id_fkey FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 ^   ALTER TABLE ONLY public.galleries_gallery DROP CONSTRAINT galleries_gallery_page_ptr_id_fkey;
       public       melissa    false    3104    221    218            j           2606    16837 &   galleries_galleryimage_gallery_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY galleries_galleryimage
    ADD CONSTRAINT galleries_galleryimage_gallery_id_fkey FOREIGN KEY (gallery_id) REFERENCES galleries_gallery(page_ptr_id) DEFERRABLE INITIALLY DEFERRED;
 g   ALTER TABLE ONLY public.galleries_galleryimage DROP CONSTRAINT galleries_galleryimage_gallery_id_fkey;
       public       melissa    false    223    3111    221            W           2606    16611 ,   generic_assignedkeyword_content_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_assignedkeyword
    ADD CONSTRAINT generic_assignedkeyword_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 n   ALTER TABLE ONLY public.generic_assignedkeyword DROP CONSTRAINT generic_assignedkeyword_content_type_id_fkey;
       public       melissa    false    3032    183    199            V           2606    16606 '   generic_assignedkeyword_keyword_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_assignedkeyword
    ADD CONSTRAINT generic_assignedkeyword_keyword_id_fkey FOREIGN KEY (keyword_id) REFERENCES generic_keyword(id) DEFERRABLE INITIALLY DEFERRED;
 i   ALTER TABLE ONLY public.generic_assignedkeyword DROP CONSTRAINT generic_assignedkeyword_keyword_id_fkey;
       public       melissa    false    199    197    3062            U           2606    16593    generic_keyword_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_keyword
    ADD CONSTRAINT generic_keyword_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 V   ALTER TABLE ONLY public.generic_keyword DROP CONSTRAINT generic_keyword_site_id_fkey;
       public       melissa    false    3045    188    197            X           2606    16624 #   generic_rating_content_type_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_rating
    ADD CONSTRAINT generic_rating_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;
 \   ALTER TABLE ONLY public.generic_rating DROP CONSTRAINT generic_rating_content_type_id_fkey;
       public       melissa    false    3032    183    201            Y           2606    16629    generic_rating_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_rating
    ADD CONSTRAINT generic_rating_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 T   ALTER TABLE ONLY public.generic_rating DROP CONSTRAINT generic_rating_user_id_fkey;
       public       melissa    false    3025    201    181            S           2606    16577 *   generic_threadedcomment_replied_to_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY generic_threadedcomment
    ADD CONSTRAINT generic_threadedcomment_replied_to_id_fkey FOREIGN KEY (replied_to_id) REFERENCES generic_threadedcomment(comment_ptr_id) DEFERRABLE INITIALLY DEFERRED;
 l   ALTER TABLE ONLY public.generic_threadedcomment DROP CONSTRAINT generic_threadedcomment_replied_to_id_fkey;
       public       melissa    false    195    3059    195            I           2606    16422    group_id_refs_id_f4b32aac    FK CONSTRAINT     �   ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;
 Z   ALTER TABLE ONLY public.auth_group_permissions DROP CONSTRAINT group_id_refs_id_f4b32aac;
       public       melissa    false    175    173    3011            t           2606    17635    page_ptr_id_refs_id_a160fcc3    FK CONSTRAINT     �   ALTER TABLE ONLY mezzanine_events_eventcontainer
    ADD CONSTRAINT page_ptr_id_refs_id_a160fcc3 FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 f   ALTER TABLE ONLY public.mezzanine_events_eventcontainer DROP CONSTRAINT page_ptr_id_refs_id_a160fcc3;
       public       melissa    false    3104    237    218            s           2606    17630    page_ptr_id_refs_id_a49ce836    FK CONSTRAINT     �   ALTER TABLE ONLY mezzanine_events_event
    ADD CONSTRAINT page_ptr_id_refs_id_a49ce836 FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 ]   ALTER TABLE ONLY public.mezzanine_events_event DROP CONSTRAINT page_ptr_id_refs_id_a49ce836;
       public       melissa    false    236    3104    218            a           2606    16785    page_ptr_id_refs_id_fe19b67b    FK CONSTRAINT     �   ALTER TABLE ONLY forms_form
    ADD CONSTRAINT page_ptr_id_refs_id_fe19b67b FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 Q   ALTER TABLE ONLY public.forms_form DROP CONSTRAINT page_ptr_id_refs_id_fe19b67b;
       public       melissa    false    210    3104    218            h           2606    16808    pages_link_page_ptr_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pages_link
    ADD CONSTRAINT pages_link_page_ptr_id_fkey FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 P   ALTER TABLE ONLY public.pages_link DROP CONSTRAINT pages_link_page_ptr_id_fkey;
       public       melissa    false    218    220    3104            f           2606    16780    pages_page_parent_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 N   ALTER TABLE ONLY public.pages_page DROP CONSTRAINT pages_page_parent_id_fkey;
       public       melissa    false    218    218    3104            e           2606    16775    pages_page_site_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pages_page
    ADD CONSTRAINT pages_page_site_id_fkey FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 L   ALTER TABLE ONLY public.pages_page DROP CONSTRAINT pages_page_site_id_fkey;
       public       melissa    false    218    188    3045            g           2606    16798 #   pages_richtextpage_page_ptr_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY pages_richtextpage
    ADD CONSTRAINT pages_richtextpage_page_ptr_id_fkey FOREIGN KEY (page_ptr_id) REFERENCES pages_page(id) DEFERRABLE INITIALLY DEFERRED;
 `   ALTER TABLE ONLY public.pages_richtextpage DROP CONSTRAINT pages_richtextpage_page_ptr_id_fkey;
       public       melissa    false    219    218    3104            N           2606    16518    site_id_refs_id_390e2add    FK CONSTRAINT     �   ALTER TABLE ONLY django_redirect
    ADD CONSTRAINT site_id_refs_id_390e2add FOREIGN KEY (site_id) REFERENCES django_site(id) DEFERRABLE INITIALLY DEFERRED;
 R   ALTER TABLE ONLY public.django_redirect DROP CONSTRAINT site_id_refs_id_390e2add;
       public       melissa    false    188    3045    185            Q           2606    16567 "   sitepermission_id_refs_id_7dccdcbd    FK CONSTRAINT     �   ALTER TABLE ONLY core_sitepermission_sites
    ADD CONSTRAINT sitepermission_id_refs_id_7dccdcbd FOREIGN KEY (sitepermission_id) REFERENCES core_sitepermission(id) DEFERRABLE INITIALLY DEFERRED;
 f   ALTER TABLE ONLY public.core_sitepermission_sites DROP CONSTRAINT sitepermission_id_refs_id_7dccdcbd;
       public       melissa    false    192    194    3056            [           2606    16680    to_blogpost_id_refs_id_6404941b    FK CONSTRAINT     �   ALTER TABLE ONLY blog_blogpost_related_posts
    ADD CONSTRAINT to_blogpost_id_refs_id_6404941b FOREIGN KEY (to_blogpost_id) REFERENCES blog_blogpost(id) DEFERRABLE INITIALLY DEFERRED;
 e   ALTER TABLE ONLY public.blog_blogpost_related_posts DROP CONSTRAINT to_blogpost_id_refs_id_6404941b;
       public       melissa    false    203    207    3085            k           2606    16861    twitter_tweet_query_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY twitter_tweet
    ADD CONSTRAINT twitter_tweet_query_id_fkey FOREIGN KEY (query_id) REFERENCES twitter_query(id) DEFERRABLE INITIALLY DEFERRED;
 S   ALTER TABLE ONLY public.twitter_tweet DROP CONSTRAINT twitter_tweet_query_id_fkey;
       public       melissa    false    225    3116    227            K           2606    16467    user_id_refs_id_40c41112    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 S   ALTER TABLE ONLY public.auth_user_groups DROP CONSTRAINT user_id_refs_id_40c41112;
       public       melissa    false    181    3025    177            M           2606    16472    user_id_refs_id_4dc23c39    FK CONSTRAINT     �   ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;
 ]   ALTER TABLE ONLY public.auth_user_user_permissions DROP CONSTRAINT user_id_refs_id_4dc23c39;
       public       melissa    false    179    3025    181            �      x������ � �      �      x������ � �      �   �  x�m�ݎ�8�����	��n3J�({����Ji<�n�О��ۯ�*����p�+�c���\=K6�cv3�u���uQ�r�T�E��/�2	B��e`Gs1v�B��UR������ �u�5�4��p�H�mZ��n6U�������8��������t��Oi��X��̾ߌ���|E�9���()��K�x� �29��8o�lU\ġ�+n�Q(O`͵��6I�n��6AG:o�$1�����z	�In�F���.uqB0U�	 ��(��k�eR�%�tQr	b�^��ai/��$7�#ҧm���C�+�j��C���Kޞ��սbʽ�ޔ}��0��e]����Ξ/O���ȁϓ�o�����*�Xz��.E(1$N�P9qF���b��5�L��ZDF=�B/�F]��ܡ���ޯBm#]�\#$t�D�O9PE���:e����OW�c]�tj*�M�(Lh)��k�u{Wy���X�Uu��yf�Z����P%��u�����]�J�]�Fa D�Eu����<��{	 ��@�"�%�A��(L!v����ܥ��������d�B���`�C�ki ؊|O;F�9��5�;�=��ή�����s��* �n�Z�"��t���D#�-�k'�f���Kf�6�BmsU_Z$۱!f�#R�������c^^U)wq׺��Q�a�@ (����qw����`�W��y��Ÿ���\K���v^q���t+"+qRQY	���Rd��{��lٯ��"+t+�� �dEHX4�l�ԍ���x�~��V�"���2��ʨ�<���u�^���Y@^�	��ND��>4rl����[)�|���/ �\�}'bNp�MO�9��W>�q�5�����M'b�A�� r"��� ��݅D�Ò��#N��[˞/ä���K�?=�hi�fS�t�>��7VB?нx6	@ad�CIj!SK����a^�?���α��� ,{�����6��������s/�      �   �   x�=��
�@ ϻO����߷��B�DүQK�0K,WvI��C1�a`�<�ե`'Yf��� @�tw�Y�鼎�zCb'�4��$n�Y��v�<l�J."cU���_�"�F�r����<p1p|�A�wM��"�Uܤ���X4y&�F*��5d��g��=Z��7(�0z      �      x������ � �      �      x������ � �            x������ � �         j   x�3�4�"4����,bN�ԢT��T�ĢTE�N#C]]CC#+SC+SK=SK3K]s����-�--@�1~ ��iS`�l��~�Po	P�+F��� ��#�            x������ � �            x������ � �      �   �  x�u��r�0E�����l�̛����X�Yj�TN�vI�׏LȄԄ7J�����DP�T��RI���@["�C�@m�,��Ն��4���#�
&� ��@q�b�V�؏���h�m��i��q�=J�se�
˩mP��u��\E;*A�D�|�/M���bTzZ/K����7�	�C%�{OX�6�7�Ÿq#p�Ƒ�k.fz�o��: �6�q?�S�V��=��1�>���z�g�/��V���cw�Ϗo����F��q��B�1͚>`5
̀54��Mh���q��-�w��e>[����Bhє�M!X3��G��i�I��]��^��P�@mz F#�u�>�4@;t�arVMs��z�Z�B5�aZ��ԙ��\������Y�a_�:� ~���|���w��&��5����i����$���*���^�Ӥc2L�0��n���8U�[�N�|5[��Ȼ}����9�
��&W���6_.�b�d�k���_IΊ\#�o�3�M�M�6���3!ϻ���K����Ѓ<-����C���_4��-A���W�J���MY�O5��G�~���u�������\�`��{�vߚ��m'�Vb�L:��p��7/����˧����Β��\���lU���ay�V��T���/^k�$ �v���|�P���^J�|�Qv      �      x������ � �      �      x������ � �           x��V�n�0<�_��Y���KѦ顗����b�E$�E������ʉ� �E'��rvgIHP��
5� �rAi�1�O�M A���E�H0�^��"��YUvYٱ���|�'HX4����ܲ����U�I����#H��N�/�ժc˴���U9[�PE�2�㠬�v�#(�Į�y1��shʡ��ʳ8�>���cV�ڍ��ߪ��|bt@���D�Sś
2����9hp�l9$$*�]�MU����۪��P�xBnУ��@Lt�U�y�`�u�>dM�c=�F]�Ml��ǰ7�8���.���ƦI�-��zw�|O�"�o�:���t�m*��hŹT0))Zy��]�x�5������
 �Z!��X���	=I��f��C�4�N����gςā_�+��iM$�Bq@�yw�^�4c�>b���NL�2�gw�>nMVRvK�!��κ}�>�D7���z�p)=��77��Xmv&ڸ���vY՛@�������/���%?�i�G3fժi�YF�P�C�28���H��G۟������#�^�w���jALCIe��N��Y�w���6n9������zs����:p\y�s��˫�(o`�+����#կXӮkZ�6A"�����4���uƕ2���.k�M�hl�"�� �����}���:!�q�����������@�1HɭA�?�Z"�A0q�4\���K��d|ӠU�i��p��Y;k��+z�I5.(>��w1�R4-E�j�z���=8,�;�x��o^c�\d@�qyݒ?|2��M ��      !      x������ � �            x������ � �      �   �  x�uSK�� >��
~Ap��Fݪj�n��*jO0�.��f}y'����{1���0��������q���LE��xrEI��Z��U���'�G�vԆ��<�;̫� w�u�\|���'
A�8�`&x��費��֕�wo�+8�q��E��n!�dG][t�>��źn��G.@�>�P���d��dx���5�2E����Ul�>�ψrI��XGHYw���� �u#�b�"������Z���9��b�[:s����ߙ���X�W��X|�(8<�g�~6����>i%$��6��"D��G9�Qy��)(���Oc����co/:r��%��Af(��&
�C��Q�4skX�}H��6}��>�(���^� �Q���)`�2��+�fU!;R�	$����^_�ц~�sN�Q|v������O��>~�� :=N$      �      x������ � �      �   s  x����n�0�u�}� lcl�kK�ՠ�$�h6@����Ҁ��ɪ�<�T��s��I�Ԍ�Lt>�)��\۞~��s�K�.�T���s�� ��$�v8\�0p���:�6*����A���˖�������ur�By2��1y�m_����V�j���m�W������c����4����?�����s(=��&�V&^��@A� H0[�d9���^Z���0&`�M�#���׊d;�j�w�dgZ)�����
�w�ճS_dE�<����+����d(!���7����
��IhO�������A���|�Ҏ��MW^��6aQ������.驾f�Ԋ�u{��@�$�ܭ��r�\/�      �   $   x�3�442�3 BC+N�Դ�Ҝ�=... _�r      
      x������ � �            x������ � �            x������ � �            x������ � �            x������ � �            x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      "   ;  x���OO� ��3����Jii��EM<�q1^v鶟-��B����j]�����x�D��^�~�u�"�A���S�L��������ݜf9�$�1SM�[�=������6�����T���Eo����v��>����ڬb�����"�
F!�8Ъ.�5�Vk��c�����ƀ�xy� �)���`����º����C����2���y�D<�RƉ�R�")��y��dH%"y��BTLD��unʃj��� �K��^ˈ���w� ���u��KG�L�	G� ���}��� �D���:�<�w��      #      x�3�L� bS ����� ��            x������ � �           x���ˎ�0���S�����k�l�h�N�nfCO@%qI۷�'�H*!Bl��}��5B�ݯ�m���7C���܀~��p��=^�.��/��)��3�Y'�S�e��Փ^�cD��I�w��Д~��Q���G�ކf(��w�me���&��?ކ����-4�uIl��	�9ɹ��-�1�41<gjY/�����E�k;�湓�Lk"a��NI���ȻkdE.�\┾]�aO�I,]s����{��7~+�]8���C�M3�	ie|�Ý$E�✉۹=c�vն���-�Hx$��	���!+���ێͶD�{Y��q:<ɜp�9���3N)�a�إ?��A���3��v���~C��T�IJ���l�q�(�eN�V��Ku���Hₔ>��8������6����Oű��uq�������lGd>����l	'$є��,�+G�"��-�bJ�.�~|P��c~���������E����\�O��?���qL&�$�gƻb*X:���ҷ�������V�~�/         /   x�3�)�J�K��U�--��K��/��2���,V ��b�=... O�         %  x��X�r�(��>ž@;�O�ϲ37����#k;��>��� �!͟d|�����a���!t�7sc�%�_	��/��ށ�A���D�Ѐ��\�Aks8�ƶ=�yn�����h$c���/烙�C�(
�����ij���K?���mo��5�űd��{��V�ܝ��NO@�2���`�5^˒ֿ���0�4�n%,L�U�OG\/XD��b����g|T�V��b�qJ8q[�gz�'w���G1 g ��&��y���W�L(�d"!�]g1�@Ff�6i�*k,�#��s,ʲ�ٳ�w癷�H�|�x��4sV��)����fI�.%v�c�LS3�ɤ����/���3��j܀ë���Q��wG�e���ޥo����z���t���h{;6�'R_�\��NQ���q�?�q���a�%W�"GIJ��4Eq�Rq&R8��Γ���?v?���t����Pަ"LU�&Mm�����SŨp�HY
���Q�
XI����6�~;?J���������r��Q�W j��)�����57sksS{)�q�aݸ�و�2�Fk�pS4ۙ��'3�
�
��e ����cqSu-\�i� lN���5aW�-=�7Z����!-*�����k(
�b�[���gw1]�qE����Z^�i��%"�=g[��K�QNj�DU�h��\����#x������D��-��'�3���U8mb�ƅ�||72m;|�֞�a ���T��0*8�S sJ}�X�ɽ��k��h��0�J)��y殐}�=E����a4��]���D���cs�Kc C�����0����/+�H6��m5Q��E�^\S�a���_wv6��q�=S� PY�Ѷ������?�"�"T<���w9ՙs��t����Ǣ,�|6�P%�����i�~sk��n���!U��a잸1�*�Ӳ��m��7����ih�?���W%��Q֑r[=���u.����w|A"�v�Z�0Q�3μv��Pð%P简ha[E�I�J���ܭ����4^*�}�]�M�p���"	�v�r�9��}�ȹ$?�,����S���0Ļ4"�e~_�H&��'���M�}��� �gYZ8���Mn���M��9�ܿ�U���;D���tɋv#�:p`W\F�G(���I"
����zs��9����%D,���'I����C���]�<�P��:�˘Ù~�2���$vx�;���1�{���q�@�TR���f�q��%���D7����!����S�]g��1}�[m�n6*�Q�3���:ܷ���^^^����Q         =   x�3�,NM,J��L+�ϵ*.I-�H͋�MNQ�M��J���K�,�2BQ�Sebh�"���� �E�         $  x�m�MK�@�s�+����dw�MC[
�R�A�2M����G7�G��M�7�9���30<�$s&g˳<	�"b"�)�B$�˂��	8/���D*ۢB����y���E[��p<��:Sb�ב�V�!Y��BK^��k"5Y`߃���p�ɪƂL�4w�Q�-(�qi(�U��D����~Ja��~J����v�ln��d\�A�f4��'���v��T���v�ōTPcG��Ʌ�.8Os��?����h7�8m��X�G۠ި�
V��,�{������yx���0��|�     